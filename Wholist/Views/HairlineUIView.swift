//
//  HairlineUIView.swift
//  Wholist
//
//  Created by Thom Schlereth on 2/6/18.
//  Copyright © 2018 Wholist. All rights reserved.
//

import UIKit

class HairlineView: UIView {
    override func awakeFromNib() {
        guard let backgroundColor = self.backgroundColor?.cgColor else { return }
        self.layer.borderColor = backgroundColor
        self.layer.borderWidth = (10.0 / UIScreen.main.scale) / 2;
        self.backgroundColor = UIColor.clear
    }
}
