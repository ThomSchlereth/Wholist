//
//  RegisterViewController.swift
//  Wholist
//
//  Created by Thom Schlereth on 2/7/18.
//  Copyright © 2018 Wholist. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    var selectedPlan: String?
    
    @IBAction func maybeLaterButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Tables", bundle: nil)
            let navigationViewController = storyboard.instantiateViewController(withIdentifier: "TablesNavigationController")
            let appDelegate = UIApplication.shared.delegate
            appDelegate?.window??.rootViewController = navigationViewController
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
