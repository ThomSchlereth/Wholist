////
////  SelectDateViewController.swift
////  Wholist
////
////  Created by Thom Schlereth on 3/30/18.
////  Copyright © 2018 Wholist. All rights reserved.
////
//
//import UIKit
//
//class SelectDateViewController: UIViewController {
//
//    var delegate: ContainerDelegateProtocol?
//
//    @IBAction func doneButtonTapped(_ sender: UIBarButtonItem) {
//        delegate?.close()
//    }
//
//    @IBAction func datePickerValueChanged(_ sender: UIDatePicker) {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "MM/dd/yy"
//        let dateString = dateFormatter.string(from: sender.date)
//        delegate?.dateChanged(dateString)
//    }
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        // Do any additional setup after loading the view.
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//}

