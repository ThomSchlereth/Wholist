////
////  SelectAddressTableViewController.swift
////  Wholist
////
////  Created by Thom Schlereth on 3/28/18.
////  Copyright © 2018 Wholist. All rights reserved.
////
//
//import UIKit
//import MapKit
//
//class SelectAddressTableViewController: UITableViewController, UISearchBarDelegate {
//
//    @IBOutlet var searchBar: UISearchBar!
//
//    let locationManager = CLLocationManager()
//    var location: CLLocation?
//    var matchingItems = [MKMapItem]()
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        searchBar.delegate = self
//        let color = UIColor(red:0.95, green:0.62, blue:0.30, alpha:1.0)
//        searchBar.setPlaceholderTextColor(color: color)
//
//        definesPresentationContext = true
//        self.navigationController?.setNavigationBarHidden(false, animated: false)
//        locationManager.delegate = self
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.requestWhenInUseAuthorization()
//        locationManager.requestLocation()
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    override func viewWillDisappear(_ animated : Bool) {
//        super.viewWillDisappear(animated)
//
//        if self.isMovingFromParentViewController {
//
//        }
//    }
//
//    // MARK: - Table view data source
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return matchingItems.count
//    }
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
//        let placemark = matchingItems[indexPath.row].placemark
//        var name = ""
//        var subThoroughfare = ""
//        var thoroughfare = ""
//        var subLocality = ""
//        var locality = ""
//        var administrativeArea = ""
//
//        if let value = placemark.name { name = value }
//        if let value = placemark.subThoroughfare { subThoroughfare = value }
//        if let value = placemark.thoroughfare { thoroughfare = value }
//        if let value = placemark.subLocality { subLocality = value }
//        if let value = placemark.locality { locality = value }
//        if let value = placemark.administrativeArea { administrativeArea = value }
//
//        let address = "\(subThoroughfare) \(thoroughfare) \(subLocality) \(locality) \(administrativeArea)"
//        cell.textLabel!.text = name
//        cell.detailTextLabel!.text = address
//        return cell
//    }
//
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let mapItem = matchingItems[indexPath.row]
//        let cell = tableView.cellForRow(at: indexPath)
//        let prevVC = self.navigationController?.viewControllers[0] as! NewListViewController
//        prevVC.mapItem = mapItem
//        prevVC.storeNameTextField.text = cell?.textLabel?.text
//        prevVC.addressTextField.text = cell?.detailTextLabel?.text
//        self.navigationController?.popToViewController(prevVC, animated: true)
//    }
//
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        let request = MKLocalSearchRequest()
//        request.naturalLanguageQuery = searchText
//        let center = CLLocationCoordinate2D(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
//        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
//        request.region = region
//        let search = MKLocalSearch(request: request)
//        search.start { response, _ in
//            guard let response = response else {
//                return
//            }
//            self.matchingItems = response.mapItems
//            self.tableView.reloadData()
//        }
//    }
//
//}
//
//extension SelectAddressTableViewController: CLLocationManagerDelegate {
//    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
//        if status == .authorizedWhenInUse {
//            self.locationManager.requestLocation()
//        }
//    }
//
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        if let newLocation = locations.first {
//            location = newLocation
//        }
//    }
//
//    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
//        print("error:: \(error)")
//    }
//}
//
//extension UISearchBar {
//
//    private func getViewElement<T>(type: T.Type) -> T? {
//        let svs = subviews.flatMap { $0.subviews }
//        guard let element = (svs.filter { $0 is T }).first as? T else { return nil }
//        return element
//    }
//
//    func getSearchBarTextField() -> UITextField? {
//        return getViewElement(type: UITextField.self)
//    }
//
//    func setPlaceholderTextColor(color: UIColor) {
//        if let textField = getSearchBarTextField() {
//            textField.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedStringKey.foregroundColor: color])
//        }
//    }
//
//}

