//
//  Member+CoreDataProperties.swift
//  Wholist
//
//  Created by Thom Schlereth on 3/20/18.
//  Copyright © 2018 Wholist. All rights reserved.
//
//

import Foundation
import CoreData


extension Member {

    @nonobjc public class func createFetchRequest() -> NSFetchRequest<Member> {
        return NSFetchRequest<Member>(entityName: "Member")
    }

    @NSManaged public var email: String
    @NSManaged public var expirationDate: NSDate
    @NSManaged public var firstName: String
    @NSManaged public var id: Int16
    @NSManaged public var lastName: String
    @NSManaged public var membershipStatus: String

}
