//
//  ViewController.swift
//  Wholist
//
//  Created by Thom Schlereth on 2/6/18.
//  Copyright © 2018 Wholist. All rights reserved.
//

import UIKit
import CoreData
import SwiftKeychainWrapper

class ViewController: UITableViewController, NSFetchedResultsControllerDelegate {

    var container: NSPersistentContainer!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        if !loggedIn() {
            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let mainNavigationViewController = storyboard.instantiateViewController(withIdentifier: "MainNavigationController")
                let appDelegate = UIApplication.shared.delegate
                appDelegate?.window??.rootViewController = mainNavigationViewController
            }
        }

        if container == nil {
            container = NSPersistentContainer(name: "Model")
            container.loadPersistentStores { storeDescription, error in
                self.container.viewContext.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy

                if let error = error {
                    print("Unresolved error \(error)")
                }
            }
        }
        let orangeColor = UIColor(red:0.95, green:0.62, blue:0.30, alpha:1.0)
        UITextField.appearance().tintColor = orangeColor

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func refreshContext() {
        container.viewContext.refreshAllObjects()
    }

    func saveContext() -> Bool {
        if container.viewContext.hasChanges {
            do {
                try container.viewContext.save()

            } catch {
                print("An error occurred while saving: \(error)")
                return false
            }
            return true
        }
        return false
    }

    func loggedIn() -> Bool {
        let retrievedJWT: String? = KeychainWrapper.standard.string(forKey: "jwt")
        var status: Bool = false
        if retrievedJWT != nil {
            status = true
        }
        return status
    }


}

