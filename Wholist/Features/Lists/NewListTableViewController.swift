//
//  NewListTableViewController.swift
//  Wholist
//
//  Created by Thom Schlereth on 3/31/18.
//  Copyright © 2018 Wholist. All rights reserved.
//

import UIKit
import MapKit

class NewListTableViewController: ViewController {

    //From Above
    var category: Category?

    //Components needed for selecting addresses
    let locationManager = CLLocationManager()
    var location: CLLocation?
    var matchingItems = [MKMapItem]()
    var mapItem: MKMapItem?

    //Set which cells should be visible
    var hideResults = true

    //Outlets from view
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var selectedDate: UILabel!
    @IBOutlet weak var storeNameTextField: UITextField!
    @IBOutlet weak var storeAddressLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mapIconButton: UIButton!

    //Actions
    @IBAction func addButtonTapped(_ sender: UIBarButtonItem) {
        addNewStore()
    }

    func addNewStore() {
        let list = List(context: self.container.viewContext)
        let store = Store(context: self.container.viewContext)

        //Add list
        if storeNameTextField?.text != "" {
            list.title = storeNameTextField!.text!
            list.id = UUID()
            list.creationDate = Date()

            //Add Category
            list.category = category!

            //Add store
            list.store = store
            if storeAddressLabel?.text != "" {
                let addressText = storeAddressLabel?.text
                store.latitude = mapItem!.placemark.coordinate.latitude as Double
                store.longitude = mapItem!.placemark.coordinate.longitude as Double
                if let addressText = addressText {
                    store.address = addressText
                }
            }

            //Add date
            if let dateText = selectedDate?.text {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/mm/yy"
                let date = dateFormatter.date(from: dateText)
                if let date = date {
                    list.dueDate = date
                }
            }
        }

        if self.saveContext() {
            if let navController = self.navigationController {
                navController.popViewController(animated: true)
            }
        } else {
            container.viewContext.delete(list)
            container.viewContext.delete(store)
            let alert = UIAlertController(title: "Uh Oh!", message: "We couldn't process that for you. Do you want to try again", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { action in
            }))
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                self.addNewStore()
            }))
            self.present(alert, animated: true, completion: nil)

        }
    }

    @IBAction func datePickerSwitchToggled(_ sender: UISwitch) {
        if sender.isOn {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yy"
            let dateString = dateFormatter.string(from: datePicker.date)
            selectedDate.text = dateString
            datePicker.isEnabled = true
        } else {
            datePicker.isEnabled = false
            selectedDate.text = "mm/dd/yy"
        }
    }
    
    @IBAction func datePickerValueChanged(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yy"
        let dateString = dateFormatter.string(from: sender.date)
        selectedDate.text = dateString
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let orangeColor = UIColor(red:0.95, green:0.62, blue:0.30, alpha:1.0)
        datePicker.setValue(orangeColor, forKey: "textColor")
        datePicker.setValue(false, forKey: "highlightsToday")
        datePicker.isEnabled = false
        tableView.tableHeaderView = searchBar
        storeNameTextField.delegate = self
        searchBar.delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        let nib = UINib.init(nibName: "StoreSearchResultCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "returnedMapItemCell")

        let origImage = UIImage(named: "icons8-map-editing-50")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        mapIconButton.setImage(tintedImage, for: .normal)
        mapIconButton.tintColor = orangeColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        let tanColor = UIColor(red:0.93, green:0.91, blue:0.80, alpha:1.0)
        header.textLabel?.textColor = tanColor
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return hideResults == true ? 0 : matchingItems.count
        }
        return super.tableView(tableView, numberOfRowsInSection: section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell: StoreSearchResultCell = tableView.dequeueReusableCell(withIdentifier: "returnedMapItemCell", for: indexPath) as! StoreSearchResultCell
            let placemark = matchingItems[indexPath.row].placemark
            var name = ""
            var subThoroughfare = ""
            var thoroughfare = ""
            var subLocality = ""
            var locality = ""
            var administrativeArea = ""

            if let value = placemark.name { name = value }
            if let value = placemark.subThoroughfare { subThoroughfare = value }
            if let value = placemark.thoroughfare { thoroughfare = value }
            if let value = placemark.subLocality { subLocality = value }
            if let value = placemark.locality { locality = value }
            if let value = placemark.administrativeArea { administrativeArea = value }

            let address = "\(subThoroughfare) \(thoroughfare) \(subLocality) \(locality) \(administrativeArea)"
            cell.textLabel!.text = name
            cell.detailTextLabel!.text = address
            cell.mapItem = matchingItems[indexPath.row]
            return cell
        }
        return super.tableView(tableView, cellForRowAt: indexPath)
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            searchItemSelected(atIndexPath: indexPath)
        }
    }

    func searchItemSelected(atIndexPath indexPath: IndexPath) {
        mapItem = matchingItems[indexPath.row]
        let cell = tableView.cellForRow(at: indexPath)
        storeAddressLabel.text = cell?.detailTextLabel?.text
        let storeAddressTapped = UITapGestureRecognizer(target: self, action: #selector(NewListTableViewController.storeAddressTapped))
        storeAddressLabel.addGestureRecognizer(storeAddressTapped)
        if storeNameTextField.text == "" {
            storeNameTextField.text = cell?.textLabel?.text
            hideResults = true
            tableView.reloadData()
        } else {
            let alert = UIAlertController(title: "Overwrite Store Name?", message: "Do you want to replace \(storeNameTextField.text ?? "") with \(cell?.textLabel?.text ?? "")", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { action in
            }))
            alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
                self.storeNameTextField.text = cell?.textLabel?.text
                self.hideResults = true
                self.tableView.reloadData()
            }))
            self.present(alert, animated: true, completion: nil)

        }
    }

    @objc func storeAddressTapped(sender:UITapGestureRecognizer) {
        self.mapItem?.openInMaps(launchOptions: nil)
    }

}

extension NewListTableViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            hideResults = true
            tableView.reloadData()
            return
        }
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = searchText
        let center = CLLocationCoordinate2D(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        request.region = region
        let search = MKLocalSearch(request: request)
        search.start { response, _ in
            guard let response = response else { return }
            self.matchingItems = response.mapItems
            self.hideResults = false
            self.tableView.reloadData()
        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if matchingItems.count > 0 {
            let indexPath = IndexPath(row: 0, section: 0)
            searchItemSelected(atIndexPath: indexPath)
        }
        self.hideResults = true
        view.endEditing(true)
        self.tableView.reloadData()
    }

}

extension NewListTableViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension NewListTableViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.locationManager.requestLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let newLocation = locations.first {
            location = newLocation
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error)")
    }
}

