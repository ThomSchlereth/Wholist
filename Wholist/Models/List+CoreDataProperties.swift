//
//  List+CoreDataProperties.swift
//  Wholist
//
//  Created by Thom Schlereth on 3/20/18.
//  Copyright © 2018 Wholist. All rights reserved.
//
//

import Foundation
import CoreData


extension List {

    @nonobjc public class func createFetchRequest() -> NSFetchRequest<List> {
        return NSFetchRequest<List>(entityName: "List")
    }

    @NSManaged public var completed: Bool
    @NSManaged public var dueDate: Date?
    @NSManaged public var id: UUID
    @NSManaged public var creationDate: Date
    @NSManaged public var title: String
    @NSManaged public var category: Category
    @NSManaged public var items: NSSet
    @NSManaged public var store: Store
}

// MARK: Generated accessors for items
extension List {

    @objc(addItemsObject:)
    @NSManaged public func addToItems(_ value: Item)

    @objc(removeItemsObject:)
    @NSManaged public func removeFromItems(_ value: Item)

    @objc(addItems:)
    @NSManaged public func addToItems(_ values: NSSet)

    @objc(removeItems:)
    @NSManaged public func removeFromItems(_ values: NSSet)

}
