//
//  NewListViewController.swift
//  Wholist
//
//  Created by Thom Schlereth on 2/19/18.
//  Copyright © 2018 Wholist. All rights reserved.
//

import UIKit
import MapKit

class NewListViewController: UIViewController {

    @IBOutlet weak var storeNameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var addListButton: UIButton!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var datePickerView: UIView!

    var category: Category?
    var mapItem: MKMapItem?

    @IBAction func addListButtonTapped(_ sender: Any) {

        if let presenter = presentingViewController as? UINavigationController {
            let store = storeNameTextField!.text
            let address = addressTextField!.text
            let date = "10-12-2018"

            let newListObject: [String: Any] = [
                "store": store as Any,
                "address": address as Any,
                "date": date,
                "phoneNumber": mapItem?.phoneNumber as Any,
                "timeZone": mapItem?.timeZone as Any,
                "mapItem": mapItem as Any
            ]

            for vc in presenter.viewControllers {
                if String(describing: type(of: vc)) == "ListTableViewController" {
                    let listController = vc as! ListTableViewController
                    listController.addNewList(newListObject: newListObject)
                }
            }
        }
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        addressTextField.delegate = self
        dateTextField.delegate = self
//        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if let category = category {
            addListButton.setTitle("Add a \(category.title) list", for: .normal)
        }

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension NewListViewController: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {

        if textField == addressTextField {
            addressTextField.resignFirstResponder()
            addressTextField.tintColor = .clear
            navigationController?.becomeFirstResponder()

            if let vc = storyboard?.instantiateViewController(withIdentifier: "SelectAddressTableViewController") {
                navigationController?.pushViewController(vc, animated: true)
            }
        } else if textField == dateTextField {
            dateTextField.resignFirstResponder()
            dateTextField.tintColor = .clear
            navigationController?.becomeFirstResponder()
            datePickerView.isHidden = false
//            if let vc = storyboard?.instantiateViewController(withIdentifier: "SelectDateViewController") {
//                navigationController?.pushViewController(vc, animated: true)
//            }

        }
    }

}

extension NewListViewController: ContainerDelegateProtocol {

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        (segue.destination as! SelectDateViewController).delegate = self
    }

    func close() {
        datePickerView.isHidden = true
    }

    func dateChanged(_ date: String) {
        dateTextField.text = date
    }

}

protocol ContainerDelegateProtocol {
    func dateChanged(_ date: String)
    func close()
}




