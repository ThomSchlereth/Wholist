//
//  Store+CoreDataProperties.swift
//  Wholist
//
//  Created by Thom Schlereth on 3/23/18.
//  Copyright © 2018 Wholist. All rights reserved.
//
//

import Foundation
import CoreData


extension Store {

    @nonobjc public class func createFetchRequest() -> NSFetchRequest<Store> {
        return NSFetchRequest<Store>(entityName: "Store")
    }

    @NSManaged public var address: String?
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var lists: NSSet

}

// MARK: Generated accessors for lists
extension Store {

    @objc(addListsObject:)
    @NSManaged public func addToLists(_ value: List)

    @objc(removeListsObject:)
    @NSManaged public func removeFromLists(_ value: List)

    @objc(addLists:)
    @NSManaged public func addToLists(_ values: NSSet)

    @objc(removeLists:)
    @NSManaged public func removeFromLists(_ values: NSSet)

}
