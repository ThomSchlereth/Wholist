//
//  CategoryTableViewController.swift
//  Wholist
//
//  Created by Thom Schlereth on 2/8/18.
//  Copyright © 2018 Wholist. All rights reserved.
//

import UIKit
import CoreData

class CategoryTableViewController: ViewController {

    let categories = ["Grocery", "Plain"]
    var fetchedResultsController: NSFetchedResultsController<Category>!

    @IBAction func SettingsMenuTapped(_ sender: Any) {
        showSettings(message: "String")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        performSelector(inBackground: #selector(fetchCategories), with: nil)
        fetchCategories()
        loadSavedData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func loadSavedData() {
        if fetchedResultsController == nil {
            let request = Category.createFetchRequest()
            let sort = NSSortDescriptor(key: "orderID", ascending: true)
            request.sortDescriptors = [sort]
            request.fetchBatchSize = 20

            fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: container.viewContext, sectionNameKeyPath: nil, cacheName: nil)
            fetchedResultsController.delegate = self
        }
        do {
            try fetchedResultsController.performFetch()
            tableView.reloadData()
        } catch {
            print("Fetch failed")
        }
    }

    func fetchCategories() {
        let titles = ["Grocery", "Plain", "Todo"]
        DispatchQueue.main.async { [unowned self] in
            for index in 0...(titles.count - 1) {
                let category = Category(context: self.container.viewContext)
                self.configure(category: category, usingTitle: titles[index], index: Int16(index))
            }
            if self.saveContext() {

            }
            self.loadSavedData()
        }
    }

    func configure(category: Category, usingTitle title: String, index: Int16) {
        category.title = title
        category.id = index
        category.orderID = index
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath)
        let category = fetchedResultsController.object(at: indexPath)
        cell.textLabel?.text = category.title
        cell.textLabel?.font = UIFont.systemFont(ofSize: 25, weight: UIFont.Weight.medium)
        cell.textLabel?.textColor = UIColor(red:0.26, green:0.29, blue:0.33, alpha:1.0)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "ListTableViewController") as? ListTableViewController {
            vc.category = fetchedResultsController.object(at: indexPath)
            vc.container = container
            navigationController?.pushViewController(vc, animated: true)
        }
    }

}
