//
//  OptionsViewController.swift
//  Wholist
//
//  Created by Thom Schlereth on 2/7/18.
//  Copyright © 2018 Wholist. All rights reserved.
//

import UIKit

class OptionsViewController: UIViewController {

    @IBAction func planSelectionButtonTapped(_ sender: Any) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as? RegisterViewController {
            vc.selectedPlan = (sender as! UIButton).titleLabel?.text
            navigationController?.pushViewController(vc, animated: true)
        }
    }

    @IBAction func maybeLaterButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Tables", bundle: nil)
            let navigationViewController = storyboard.instantiateViewController(withIdentifier: "TablesNavigationController")
            let appDelegate = UIApplication.shared.delegate
            appDelegate?.window??.rootViewController = navigationViewController
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
