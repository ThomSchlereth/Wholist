
//  ListTableViewController.swift
//  Wholist
//
//  Created by Thom Schlereth on 2/10/18.
//  Copyright © 2018 Wholist. All rights reserved.
//

import UIKit
import CoreData
import MapKit

class ListTableViewController: ViewController {

    var category: Category?
    var listPredicate: NSPredicate?
    var fetchedResultsController: NSFetchedResultsController<List>!

    @IBAction func addButtonTapped(_ sender: UIBarButtonItem) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "NewListTableViewController") as? NewListTableViewController {
            vc.container = container
            vc.category = category
            navigationController?.pushViewController(vc, animated: true)
        }
    }

    @IBAction func SettingsButtonTapped(_ sender: Any) {
        showSettings(message: "String")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = category?.title
        self.listPredicate = NSPredicate(format: "category.id == %@", NSNumber(value: Int(category!.id)))
        let nib = UINib.init(nibName: "GroceryListCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "groceryListCell")
        loadSavedData()
    }

    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func loadSavedData() {
        if fetchedResultsController == nil {
            let request = List.createFetchRequest()
            let sort = NSSortDescriptor(key: "creationDate", ascending: false)
            request.sortDescriptors = [sort]
            request.fetchBatchSize = 20

            fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: container.viewContext, sectionNameKeyPath: nil, cacheName: nil)
            fetchedResultsController.delegate = self
        }
        fetchedResultsController.fetchRequest.predicate = listPredicate
        do {
            try fetchedResultsController.performFetch()
            tableView.reloadData()
        } catch {
            print("Fetch failed")
        }
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
        default:
            break
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: GroceryListCell = tableView.dequeueReusableCell(withIdentifier: "groceryListCell", for: indexPath) as! GroceryListCell
        cell.backgroundColor = .clear
        let list = fetchedResultsController.object(at: indexPath)
        let coordinates = CLLocationCoordinate2DMake(list.store.latitude, list.store.longitude)
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "\(list.title)"
        cell.mapItem = mapItem


        cell.storeNameLabel?.text = list.title

        cell.addressLabel?.text = list.store.address ?? ""

        if let dueDate = list.dueDate {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/mm/yy"
            let date = dateFormatter.string(from: dueDate)
            cell.dateLabel?.text = date
        } else {
            cell.dateLabel?.text = ""
        }

        if completed(list: list) == true {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }

        cell.tintColor = UIColor(red:0.26, green:0.29, blue:0.33, alpha:1.0)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "ItemTableViewController") as? ItemTableViewController {
            vc.list = fetchedResultsController.object(at: indexPath)
            vc.container = container
            navigationController?.pushViewController(vc, animated: true)
        }
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let list = fetchedResultsController.object(at: indexPath)
            container.viewContext.delete(list)
            if saveContext() {
                
            }
        }
    }

    //helper functions
    func completed(list: List) -> Bool {
        for item in list.items {
            let item = item as! Item
            if item.purchasedCount != item.neededCount {
                return false
            }
        }
        return true
    }
}
