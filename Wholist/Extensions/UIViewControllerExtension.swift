//
//  UIViewControllerExtension.swift
//  Wholist
//
//  Created by Thom Schlereth on 2/11/18.
//  Copyright © 2018 Wholist. All rights reserved.
//

import UIKit

extension UIViewController {

    func showSettings(message : String) {
        let alertController = SettingsAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        self.present(alertController, animated: true, completion: nil)
    }



}
