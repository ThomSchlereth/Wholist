//
//  Item+CoreDataProperties.swift
//  Wholist
//
//  Created by Thom Schlereth on 3/20/18.
//  Copyright © 2018 Wholist. All rights reserved.
//
//

import Foundation
import CoreData


extension Item {

    @nonobjc public class func createFetchRequest() -> NSFetchRequest<Item> {
        return NSFetchRequest<Item>(entityName: "Item")
    }

    @NSManaged public var id: UUID
    @NSManaged public var title: String
    @NSManaged public var creationDate: Date
    @NSManaged public var purchasedCount: Int16
    @NSManaged public var neededCount: Int16
    @NSManaged public var list: List

}
