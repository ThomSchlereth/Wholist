//
//  StoreSearchResultCell.swift
//  Wholist
//
//  Created by Thom Schlereth on 4/1/18.
//  Copyright © 2018 Wholist. All rights reserved.
//

import UIKit
import MapKit

class StoreSearchResultCell: UITableViewCell {

    @IBOutlet weak var storeNameLabel: UILabel!
    @IBOutlet weak var storeAddressLabel: UILabel!
    var mapItem: MKMapItem?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let storeAddressTapped = UITapGestureRecognizer(target: self, action: #selector(StoreSearchResultCell.storeAddressTapped))
        storeAddressLabel.addGestureRecognizer(storeAddressTapped)
    }

    @objc func storeAddressTapped(sender:UITapGestureRecognizer) {
        self.mapItem?.openInMaps(launchOptions: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
