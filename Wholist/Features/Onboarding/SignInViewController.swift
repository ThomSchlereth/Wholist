//
//  SignInViewController.swift
//  Wholist
//
//  Created by Thom Schlereth on 2/7/18.
//  Copyright © 2018 Wholist. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftKeychainWrapper

class SignInViewController: UIViewController {

    @IBOutlet weak var emailAddressTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    @IBAction func registerButtonTapped(_ sender: Any) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "OptionsViewController") as? OptionsViewController {
            navigationController?.pushViewController(vc, animated: true)
        }
    }

    @IBAction func signInButtonTapped(_ sender: Any) {
        var auth: Parameters = [:]
        if let emailAddress = emailAddressTextField.text {
            auth["email"] = emailAddress
        }
        if let password = passwordTextField.text {
            auth["password"] = password

        }

        let parameters: Parameters = [
            "auth": auth
        ]

        Alamofire.request("http://localhost:3000/api/v1/sessions", method: .post, parameters: parameters)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                self.passwordTextField.text = ""
                switch response.result {
                case .success:
                    let saveSuccessful: Bool = KeychainWrapper.standard.set("", forKey: "jwt")
                    if saveSuccessful {
                        DispatchQueue.main.async {
                            let storyboard = UIStoryboard(name: "Tables", bundle: nil)
                            let navigationViewController = storyboard.instantiateViewController(withIdentifier: "TablesNavigationController")
                            let appDelegate = UIApplication.shared.delegate
                            appDelegate?.window??.rootViewController = navigationViewController
                        }
                    }
                case .failure(let error):
                    print("error: \(error)")
                    if let statusCode = response.response?.statusCode {
                        print("statusCode: \(statusCode)")
                    } else {
                        print("statusCode: Unknown")
                    }
                }
            }
    }

    @IBAction func forgotPasswordButtonTapped(_ sender: Any) {
    }

    @IBAction func maybeLaterButtonTapped(_ sender: Any) {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Tables", bundle: nil)
            let navigationViewController = storyboard.instantiateViewController(withIdentifier: "TablesNavigationController")
            let appDelegate = UIApplication.shared.delegate
            appDelegate?.window??.rootViewController = navigationViewController
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func dateIn(numMonths months: Int) -> Date {
        let currentDate = Date()
        var dateComponent = DateComponents()
        dateComponent.month = months

        return Calendar.current.date(byAdding: dateComponent, to: currentDate)!
    }

}
