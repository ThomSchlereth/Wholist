//
//  GroceryItemCell.swift
//  Wholist
//
//  Created by Thom Schlereth on 4/2/18.
//  Copyright © 2018 Wholist. All rights reserved.
//

import UIKit

class GroceryItemCell: UITableViewCell {

    var delegate: GroceryItemProtocol?

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var neededCount: UILabel!
    @IBOutlet weak var purchasedCount: UILabel!
    @IBOutlet weak var purchasedCountStepper: UIStepper!

    @IBAction func purchasedCountChanged(_ sender: UIStepper) {
        let newValue = Int16(sender.value)
        delegate?.stepperChanged(toNewValue: newValue, cell: self)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

protocol GroceryItemProtocol {
    func stepperChanged(toNewValue value: Int16, cell: GroceryItemCell)
}
