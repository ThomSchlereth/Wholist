//
//  GroceryItemTableVC.swift
//  Wholist
//
//  Created by Thom Schlereth on 4/2/18.
//  Copyright © 2018 Wholist. All rights reserved.
//

import UIKit
import CoreData

class ItemTableViewController: ViewController {

    var list: List?
    var itemPredicate: NSPredicate?
    var fetchedResultsController: NSFetchedResultsController<Item>!
    var filterCompletedItems = false

    @IBAction func filterCompletedItemsTapped(_ sender: UIBarButtonItem) {
        filterCompletedItems = !filterCompletedItems
        tableView.reloadData()
    }

    @IBAction func addItemButtonTapped(_ sender: UIBarButtonItem) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "NewItemTableViewController") as? NewItemTableViewController {
            vc.container = container
            vc.list = list
            navigationController?.pushViewController(vc, animated: true)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = list?.title
        self.itemPredicate = NSPredicate(format: "list.id == %@", list!.id as CVarArg)
        let nib = UINib.init(nibName: "GroceryItemCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "groceryItemCell")
        loadSavedData()
    }

    func loadSavedData() {
        if fetchedResultsController == nil {
            let request = Item.createFetchRequest()
            let sort = NSSortDescriptor(key: "creationDate", ascending: false)
            request.sortDescriptors = [sort]
            request.fetchBatchSize = 20
            fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: container.viewContext, sectionNameKeyPath: nil, cacheName: nil)
            fetchedResultsController.delegate = self
        }
        fetchedResultsController.fetchRequest.predicate = itemPredicate
        do {
            try fetchedResultsController.performFetch()
            tableView.reloadData()
        } catch {
            print("Fetch failed")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections

        return fetchedResultsController.sections?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let sectionInfo = fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "groceryItemCell", for: indexPath) as! GroceryItemCell
        let item = fetchedResultsController.object(at: indexPath)
        cell.titleLabel?.text = item.title
        cell.purchasedCount.text = String(item.purchasedCount)
        cell.neededCount.text = String(item.neededCount)
        cell.delegate = self
        cell.purchasedCountStepper.value = Double(item.purchasedCount)
        cell.purchasedCountStepper.maximumValue = Double(item.neededCount)

        if item.neededCount == item.purchasedCount {
            if filterCompletedItems {
                cell.isHidden = true
            } else {
                let green = UIColor(red:0.58, green:0.73, blue:0.56, alpha:1.0)
                cell.purchasedCount.textColor = green
                cell.neededCount.textColor = green
            }
        } else {
            let red = UIColor(red:0.73, green:0.38, blue:0.38, alpha:1.0)
            cell.purchasedCount.textColor = red
            cell.neededCount.textColor = red
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = fetchedResultsController.object(at: indexPath)
        if filterCompletedItems && item.neededCount == item.purchasedCount {
            return 0.0
        }
        return 92.0
    }

    override func viewDidAppear(_ animated: Bool) {
        loadSavedData()
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let item = fetchedResultsController.object(at: indexPath)
            container.viewContext.delete(item)
            if saveContext() {

            }
        }
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
        default:
            break
        }
    }

}

extension ItemTableViewController: GroceryItemProtocol {

    func stepperChanged(toNewValue value: Int16, cell: GroceryItemCell) {
        let indexPath = tableView.indexPath(for: cell)
        let item = fetchedResultsController.object(at: indexPath!)
        item.setValue(value, forKeyPath: "purchasedCount")
        saveContext()
        tableView.reloadData()
    }
}
