//
//  GroceryListTableViewCell.swift
//  Wholist
//
//  Created by Thom Schlereth on 3/21/18.
//  Copyright © 2018 Wholist. All rights reserved.
//

import UIKit
import MapKit

class GroceryListCell: UITableViewCell {

    @IBOutlet weak var storeNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    var mapItem: MKMapItem?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let mapAddressTapped = UITapGestureRecognizer(target: self, action: #selector(GroceryListCell.mapAddressTapped))
        addressLabel.addGestureRecognizer(mapAddressTapped)
        storeNameLabel.adjustsFontSizeToFitWidth = false
        storeNameLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
    }

    @objc func mapAddressTapped(sender:UITapGestureRecognizer) {
        self.mapItem?.openInMaps(launchOptions: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
