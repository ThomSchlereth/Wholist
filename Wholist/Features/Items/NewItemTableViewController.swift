//
//  NewItemTableViewController.swift
//  Wholist
//
//  Created by Thom Schlereth on 4/6/18.
//  Copyright © 2018 Wholist. All rights reserved.
//

import UIKit

class NewItemTableViewController: ViewController {

    var list: List?
    
    @IBOutlet weak var quantityPurchased: UITextField!
    @IBOutlet weak var quantityNeeded: UITextField!
    @IBOutlet weak var neededStepper: UIStepper!
    @IBOutlet weak var purchasedStepper: UIStepper!
    @IBOutlet weak var titleField: UITextField!

    @IBAction func purchasedStepperTapped(_ sender: UIStepper) {
        quantityPurchased.text = String(Int(sender.value))
    }

    @IBAction func neededStepperTapped(_ sender: UIStepper) {
        quantityNeeded.text = String(Int(sender.value))
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.title = "Add a new item"
        let addButton : UIBarButtonItem = UIBarButtonItem(title: "Add", style: UIBarButtonItemStyle.plain, target: self, action: #selector(NewItemTableViewController.addItem))
        self.navigationItem.rightBarButtonItem = addButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func addItem() {
        let item = Item(context: self.container.viewContext)
        item.id = UUID()
        item.creationDate = Date()
        item.list = list!
        item.title = titleField.text!
        item.neededCount = Int16(quantityNeeded.text!)!
        item.purchasedCount = Int16(quantityPurchased.text!)!
        if self.saveContext() {
            if let navController = self.navigationController {
                navController.popViewController(animated: true)
            }
        }
    }
}
