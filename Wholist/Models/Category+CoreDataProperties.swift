//
//  Category+CoreDataProperties.swift
//  Wholist
//
//  Created by Thom Schlereth on 3/20/18.
//  Copyright © 2018 Wholist. All rights reserved.
//
//

import Foundation
import CoreData


extension Category {

    @nonobjc public class func createFetchRequest() -> NSFetchRequest<Category> {
        return NSFetchRequest<Category>(entityName: "Category")
    }

    @NSManaged public var id: Int16
    @NSManaged public var orderID: Int16
    @NSManaged public var title: String
    @NSManaged public var lists: NSSet

}

// MARK: Generated accessors for lists
extension Category {

    @objc(addListsObject:)
    @NSManaged public func addToLists(_ value: List)

    @objc(removeListsObject:)
    @NSManaged public func removeFromLists(_ value: List)

    @objc(addLists:)
    @NSManaged public func addToLists(_ values: NSSet)

    @objc(removeLists:)
    @NSManaged public func removeFromLists(_ values: NSSet)

}
