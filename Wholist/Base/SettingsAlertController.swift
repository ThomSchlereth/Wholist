//
//  SettingsAlertController.swift
//  Wholist
//
//  Created by Thom Schlereth on 2/13/18.
//  Copyright © 2018 Wholist. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class SettingsAlertController: UIAlertController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        self.addAction(UIAlertAction(title: NSLocalizedString("Sign Out", comment: "App Settings"), style: .destructive, handler: { _ in
            let removeSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: "jwt")
            if removeSuccessful {
                DispatchQueue.main.async {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let mainNavigationViewController = storyboard.instantiateViewController(withIdentifier: "MainNavigationController")
                    let appDelegate = UIApplication.shared.delegate
                    appDelegate?.window??.rootViewController = mainNavigationViewController
                }
            }
        }))
        self.addAction(UIAlertAction(title: NSLocalizedString("Account Settings", comment: "App Settings"), style: .default, handler: { _ in

        }))
        self.addAction(UIAlertAction(title: NSLocalizedString("App Settings", comment: "App Settings"), style: .default, handler: { _ in

        }))
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Default action"), style: .cancel, handler: nil )
        self.addAction(cancelAction)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
